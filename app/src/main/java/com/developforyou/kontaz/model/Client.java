package com.developforyou.kontaz.model;

import com.developforyou.kontaz.config.config_firebase;
import com.developforyou.kontaz.helper.base64custom;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;

public class Client{

    private String nomeCliente, email, data, produto;
    private Double compra=0.00, pagamento=0.00;


    public void salvar(){

        FirebaseAuth auth = config_firebase.getAuth();
        String idUsuario = base64custom.codificarbase64(auth.getCurrentUser().getEmail());
        DatabaseReference ref = config_firebase.getDatabase();
        ref.child("usuarios").child(idUsuario).child("clientes").child(this.nomeCliente).setValue(this);

    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Exclude public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    @Exclude public Double getCompra() {
        return compra;
    }

    public void setCompra(Double compra) {
        this.compra = compra;
    }

    @Exclude public Double getPagamento() {
        return pagamento;
    }

    public void setPagamento(Double pagamento) {
        this.pagamento = pagamento;
    }
}
