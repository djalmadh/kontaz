package com.developforyou.kontaz.model;

import com.developforyou.kontaz.config.config_firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;

public class usuario {
    private String nome, senha, email, idUsuario;

    public usuario() {
    }

    public void salvar(){
        DatabaseReference ref = config_firebase.getDatabase();
        ref.child("usuarios").child(this.idUsuario).setValue(this);
    }

    @Exclude public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    @Exclude public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
