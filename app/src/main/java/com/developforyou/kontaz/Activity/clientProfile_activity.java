package com.developforyou.kontaz.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.developforyou.kontaz.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class clientProfile_activity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_profile_activity);

        FloatingActionButton fab = findViewById(R.id.fab_clientAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startnewActivity();
            }
        });
    }
    public void startnewActivity(){
        Intent i = new Intent(this,addClientDebits_activity.class );
        startActivity(i);
    }
}
