package com.developforyou.kontaz.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.developforyou.kontaz.Activity.cadastrar_activity;
import com.developforyou.kontaz.Activity.login_activity;
import com.developforyou.kontaz.R;
import com.developforyou.kontaz.config.config_firebase;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        verify_currentUser();
    }

    public void btEntrar(View view){
        startActivity(new Intent(this, login_activity.class));
    }

    public void btCadastrar(View view){
        startActivity(new Intent(this, cadastrar_activity.class));
    }

    public void verify_currentUser(){
        auth = config_firebase.getAuth();
       // auth.signOut();
        if(auth.getCurrentUser() != null){
            startActivity(new Intent(this, initialScreen_activity.class));
        }
    }
}
