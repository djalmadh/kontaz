package com.developforyou.kontaz.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.developforyou.kontaz.R;
import com.developforyou.kontaz.config.config_firebase;
import com.developforyou.kontaz.helper.base64custom;
import com.developforyou.kontaz.model.usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

public class cadastrar_activity extends AppCompatActivity {

    private EditText campoEmail, campoSenha,campoNome;
    private Button botaoCadastrar;
    private FirebaseAuth auth;
    private usuario user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_activity);

        campoEmail = findViewById(R.id.editText_emailCadastro);
        campoNome = findViewById(R.id.editText_nomeCadastro);
        campoSenha = findViewById(R.id.editText_senhaCadastro);
        botaoCadastrar = findViewById(R.id.button_confirmarCadastro);

        //Botao responsavel pelo cadastro do usuario
        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nome = campoNome.getText().toString();
                String senha = campoSenha.getText().toString();
                String email = campoEmail.getText().toString();

                if(!nome.isEmpty()){
                    if(!email.isEmpty()){
                        if (!senha.isEmpty()){
                            user = new usuario();
                            user.setNome(nome);
                            user.setEmail(email);
                            user.setSenha(senha);
                            cadastrar_usuario();
                        }
                        else{
                            Toast.makeText(cadastrar_activity.this, "Preencha a Senha", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(cadastrar_activity.this, "Preencha o Email!", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(cadastrar_activity.this, "Preencha o Nome!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //metodo responsavel pelo cadastro do usuario
    public void cadastrar_usuario(){
        auth = config_firebase.getAuth();
        auth.createUserWithEmailAndPassword(user.getEmail(), user.getSenha()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                    String idUsuario = base64custom.codificarbase64(user.getEmail());
                    user.setIdUsuario(idUsuario);
                    user.salvar();
                    finish();
                }
                else {
                    String ex = "";
                    try{
                        throw task.getException();
                    }catch(FirebaseAuthWeakPasswordException e){
                        ex = "Digite uma Senha mais forte!";
                    }catch (FirebaseAuthInvalidCredentialsException e){
                        ex = "Digite um E-mail válido!";
                    }catch(FirebaseAuthUserCollisionException e){
                        ex = "Conta já cadastrada!";
                    }catch (Exception e){
                        ex = "Error ao cadastrar usuário: " + e.getMessage();
                        e.printStackTrace();
                    }

                    Toast.makeText(cadastrar_activity.this, ex, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
