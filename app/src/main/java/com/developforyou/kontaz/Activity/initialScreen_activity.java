package com.developforyou.kontaz.Activity;

import android.content.Intent;
import android.os.Bundle;

import com.developforyou.kontaz.config.config_firebase;
import com.developforyou.kontaz.helper.RecyclerItemClickListener;
import com.developforyou.kontaz.helper.adapterClientes;
import com.developforyou.kontaz.helper.base64custom;
import com.developforyou.kontaz.model.Client;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.developforyou.kontaz.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class initialScreen_activity extends AppCompatActivity {

    private RecyclerView recyclerClientes;
    private List<Client> clients = new ArrayList<>();
    private adapterClientes adapterClientes;
    private DatabaseReference refClients;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_screen_activity);

        //pega inf de users
        FirebaseAuth auth = config_firebase.getAuth();
        String idUsuario = base64custom.codificarbase64(auth.getCurrentUser().getEmail());

        refClients = config_firebase.getDatabase().child("usuarios").child(idUsuario).child("clientes");
        recyclerClientes = findViewById(R.id.recycler_clientes);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               fab_addClient();
            }
        });

        //lista de clientes
        recyclerClientes.setLayoutManager(new LinearLayoutManager(this));
        recyclerClientes.setHasFixedSize(true);
        adapterClientes = new adapterClientes(clients,this);
        recyclerClientes.setAdapter(adapterClientes);

        //recupera clientes
        rec_clients();

        //evento de clique
        recyclerClientes.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), recyclerClientes, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                startNew_activity();
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        }));

    }

    public void fab_addClient(){
        startActivity(new Intent(this, add_client_activity.class));
    }

    private void rec_clients(){
        refClients.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                clients.clear();
                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    clients.add(ds.getValue(Client.class));
                }

                Collections.reverse(clients);
                adapterClientes.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void startNew_activity()
    {
        Intent i = new Intent(this, clientProfile_activity.class);
        startActivity(i);
    }
}
