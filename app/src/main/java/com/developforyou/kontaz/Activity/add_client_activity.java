package com.developforyou.kontaz.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.developforyou.kontaz.R;
import com.developforyou.kontaz.helper.dateUtil;
import com.developforyou.kontaz.model.Client;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

public class add_client_activity extends AppCompatActivity {

    private TextInputEditText campoNome,campoEmail,campoData;
    private FloatingActionButton fabButton;
    private Client client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_client_activity);

        campoNome = findViewById(R.id.editText_newClientName);
        campoEmail = findViewById(R.id.editText_newClientEmail);
        campoData = findViewById(R.id.editText_newClientData);
        fabButton = findViewById(R.id.fab_newClient);

        campoData.setText(dateUtil.dataAtual());

        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nome = campoNome.getText().toString();
                String data = campoData.getText().toString();
                String email = campoEmail.getText().toString();

                if(!nome.isEmpty()){
                    if (!data.isEmpty()){
                        client = new Client();
                        client.setNomeCliente(nome);
                        client.setEmail(email);
                        client.setData(data);
                        client.salvar();
                    }
                    else{
                        Toast.makeText(add_client_activity.this, "Preencha uma Data", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(add_client_activity.this, "Preencha o Nome!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void registrarClient(){

    }


}
