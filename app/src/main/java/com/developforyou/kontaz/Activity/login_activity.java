package com.developforyou.kontaz.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.developforyou.kontaz.R;
import com.developforyou.kontaz.config.config_firebase;
import com.developforyou.kontaz.model.usuario;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;

public class login_activity extends AppCompatActivity {
    private EditText campoEmail , campoSenha;
    private Button buttonEntrar;
    private usuario user;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_activity);

        campoEmail = findViewById(R.id.editText_emailLogin);
        campoSenha = findViewById(R.id.editText_senhaLogin);
        buttonEntrar = findViewById(R.id.button_confirmarLogin);

        buttonEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = campoEmail.getText().toString(),
                        senha = campoSenha.getText().toString();

                if(!email.isEmpty()){
                    if (!senha.isEmpty()){
                        user = new usuario();
                        user.setEmail(email);
                        user.setSenha(senha);
                        validar_login();

                    }
                    else{
                        Toast.makeText(login_activity.this, "Preencha a Senha", Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(login_activity.this, "Preencha o Email!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void validar_login(){
        auth = config_firebase.getAuth();
        auth.signInWithEmailAndPassword(
                user.getEmail(), user.getSenha()
        ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    abrirTela_inicial();
                }
                else{
                    String ex = "";
                    try{
                        throw task.getException();
                    }catch(FirebaseAuthInvalidUserException e){
                        ex = "Usuário não cadastrado";
                    }catch(FirebaseAuthInvalidCredentialsException e){
                        ex = "E-mail e/ou senha incorretos";
                    }catch (Exception e){
                        ex = "Error ao cadastrar usuário: " + e.getMessage();
                        e.printStackTrace();
                    }

                    Toast.makeText(login_activity.this, ex, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void abrirTela_inicial(){
        startActivity(new Intent(this, initialScreen_activity.class));
        finish();
    }
}
