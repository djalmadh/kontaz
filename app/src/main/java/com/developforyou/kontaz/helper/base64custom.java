package com.developforyou.kontaz.helper;

import android.util.Base64;

public class base64custom {
    public static String codificarbase64(String text){
        return Base64.encodeToString(text.getBytes(), Base64.DEFAULT).replaceAll("(\\n|\\r)", "");
    }

    public static String decodificarbase64(String text_codificado){
        return  new String(Base64.decode(text_codificado , Base64.DEFAULT));
    }
}
