package com.developforyou.kontaz.helper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.developforyou.kontaz.R;
import com.developforyou.kontaz.model.Client;

import java.util.List;

public class adapterClientes extends RecyclerView.Adapter<adapterClientes.MyViewHolder > {

    private List<Client> clients;
    private Context context;

    public adapterClientes(List<Client> clients, Context context) {
        this.clients = clients;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.adpter_client, parent, false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Client client = clients.get(position);
        holder.nome.setText(client.getNomeCliente());
        holder.data_cadastro.setText(client.getData());
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView nome, data_cadastro;
        ImageView iconClient;

        public MyViewHolder(View view){
            super(view);

            nome = view.findViewById(R.id.textView_nomeList);
            data_cadastro = view.findViewById(R.id.textView_dataList);
            iconClient = view.findViewById(R.id.imageView_list);

        }
    }
}
