package com.developforyou.kontaz.config;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class config_firebase {

    private static FirebaseAuth auth;
    private static DatabaseReference refData;

    public static DatabaseReference getDatabase(){
        if(refData == null){
            refData = FirebaseDatabase.getInstance().getReference();
        }
        return refData;
    }

    public static FirebaseAuth getAuth(){
        if(auth == null) {
            auth = FirebaseAuth.getInstance();
        }
        return auth;
    }
}
